interface LabelledValue {
    label: string;
}

function printLabel(labelObj: LabelledValue) {
    console.log('labelObj.label:', labelObj.label);
}

let myObj1 = {
    name: 'rajesh',
    label: 'Developer'
};
printLabel(myObj1);