function identity(arg) {
    return arg;
}
var output1 = identity(123);
var output2 = identity(123);
function loggingIdentity(arg) {
    console.log('arguments.length:', arg.length);
    return arg;
}
var inputArray = [1, 2, 3];
var outputArray1 = loggingIdentity(inputArray);
var outputArray2 = loggingIdentity([1, 2, 3]);
