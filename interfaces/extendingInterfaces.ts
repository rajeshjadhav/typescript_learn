interface Shape {
    color: string;
}
interface Square extends Shape {
    sideLength: number;
}

// type assertion
// angle-bracket syntax
// let square = <Square>{};

// as syntax
let square = {} as Square;

square.color = "blue";
square.sideLength = 10;