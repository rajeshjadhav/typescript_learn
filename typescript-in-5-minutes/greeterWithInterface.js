function greeter(person) {
    return 'Hello, ' + person.fname + ' ' + person.lname;
}
var user = {
    fname: 'Rajesh',
    lname: 'Jadhav'
};
document.body.innerHTML = greeter(user);
