function buildName(firstName: string, lastName?: string, prefix="Mr.") {
    if (lastName)
        return prefix + firstName + " " + lastName;
    else
        return firstName;
}

let result1 = buildName("Bob");                  // works correctly now
let result3 = buildName("Bob", "Adams");         // ah, just right

console.log('result1:', result1);

console.log('result3:', result3);