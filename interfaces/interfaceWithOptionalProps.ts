interface LabelledValue {
    name?: string,
    label?: string;
}

function printLabel(labelObj: LabelledValue): { name: string; city: string } {
    let newobj = { name: "rajesh", city: "Sangli" };
    if (labelObj.name) {
        newobj.name = labelObj.name;
    }
    if (labelObj.label) {
        newobj.label = labelObj.label;
    }
    return newobj;
}

printLabel({
    name: "rajesh"
});