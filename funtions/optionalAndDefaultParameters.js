function buildName(firstName, lastName, prefix) {
    if (prefix === void 0) { prefix = "Mr."; }
    if (lastName)
        return prefix + firstName + " " + lastName;
    else
        return firstName;
}
var result1 = buildName("Bob");
var result3 = buildName("Bob", "Adams");
console.log('result1:', result1);
console.log('result3:', result3);
