// Boolean
let isDone: boolean = false;

// Number
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;

// String
let color: string = "blue";
color = 'red';

// Array
// 1st way of writting
let list1: number[] = [1, 2, 3];

// 2nd way of writting
let list2: Array<number> = [1, 2, 3];

// Tuple
// Declare a tuple type
let x: [string, number];
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
// x = [10, "hello"]; // Error

// x[3] = "world"; // OK, 'string' can be assigned to 'string | number'

// console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'

// x[6] = true; // Error, 'boolean' isn't 'string | number'

// Enum
enum Color { Red = 1, Green, Blue }
let colorName: string = Color[2];

console.log(colorName); // Displays 'Green' as its value is 2 above


// Any
let notSure: any = 5
notSure = 'rajesh jadhav'
console.log('notSure:', notSure);
notSure = true
console.log('notSure:', notSure);

// Void
function warnUser(): void {
    console.log("This is my warning message");
}

let unusable: void = undefined;

// Null and Undefined
// Not much else we can assign to these variables!
let u: undefined = undefined;
let n: null = null;

// Object
// declare function create(o: object | null): void;

// create({ prop: 0 }); // OK
// create(null); // OK

// create(42); // Error
// create("string"); // Error
// create(false); // Error
// create(undefined); // Error

// Type assertions
// Type assertions have two forms.

// One is the “angle-bracket” syntax:
let someValue: any = "i am somevalue"

let strLength1: Number = (<string>someValue).length
console.log('strLength1:',strLength1);

// And the other is the as-syntax:
let strLength2: Number = (someValue as string).length
console.log('strLength2:',strLength2);

//*** when using TypeScript with JSX, 
// only as-style assertions are allowed.

