function getProperty<T, K extends keyof T>(obj: T, key: K) {
    return obj[key];
}

let x1 = { a: 1, b: 2, c: 3, d: 4 };

console.log(getProperty(x1, "a")); // okay
// getProperty(x, "m"); // error: Argument of type 'm' isn't assignable to 'a' | 'b' | 'c' | 'd'.
