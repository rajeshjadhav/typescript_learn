/**
 * Generics
 * TypeScript enables you to write code that can act on a variety of 
 * data types instead of being limited to a single one.
 * 
 */
function identity<T>(arg: T): T {
    return arg;
}

//1st way - pass arguments including type of argument
let output1 = identity<number>(123);

// 2nd way - type argument inference 
let output2 = identity(123);




function loggingIdentity<T>(arg: Array<T>): Array<T> {
    console.log('arguments.length:',arg.length);
    return arg;
}

let inputArray: number[] = [1, 2, 3];
//1st way - pass arguments including type od argument
let outputArray1 : number[]= loggingIdentity(inputArray);

// 2nd way - type argument inference 
let outputArray2 = loggingIdentity([1, 2, 3]);