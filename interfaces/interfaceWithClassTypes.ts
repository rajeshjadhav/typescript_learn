interface ClockInterface {
    currentTime: Date;
}

class Clock implements ClockInterface {
    currentTime: Date;
    setTime(d: Date) {
        this.currentTime = d;
        console.log('currentTime:',this.currentTime);
    }
    constructor(h: number, m: number) { }
}
let clockObj = new Clock(1,20);

clockObj.setTime(new Date);