var Student = (function () {
    function Student(fname, lname) {
        this.fname = fname;
        this.lname = lname;
        this.fullName = fname + ' ' + lname;
    }
    return Student;
}());
function greeter(person) {
    return 'Hello, ' + person.fname + ' ' + person.lname;
}
var user = {
    fname: 'Rajesh',
    lname: 'Jadhav'
};
document.body.innerHTML = greeter(user);
