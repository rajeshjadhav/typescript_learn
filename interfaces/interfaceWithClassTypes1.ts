// Difference between the static and instance sides of classes
// A class has two types: the type of the static side 
// and the type of the instance side. 
// You may notice that if you create an interface with a construct signature 
// and try to create a class that implements this interface you get an error:

/*interface ClockConstructor {
    new (hour: number, minute: number);
}

class Clock implements ClockConstructor {
    currentTime: Date;
    constructor(h: number, m: number) { }
}
*/

// when a class implements an interface, 
// only the instance side of the class is checked. 
// Since the constructor sits in the static side, it is not included in this check. 

interface ClockConstructor {
    new(hour: number, minute: number): ClockInterface1;
}
interface ClockInterface1 {
    tick();
}

function createClock(ctor: ClockConstructor, hour: number, minute: number): ClockInterface1 {
    return new ctor(hour, minute);
}

class DigitalClock implements ClockInterface1 {
    constructor(h: number, m: number) {
        console.log(h, m);
    }
    tick() {
        console.log("beep beep");
    }
}
class AnalogClock implements ClockInterface1{
    constructor(h: number, m: number) {
        console.log(h, m);
    }
    tick() {
        console.log("tick tock");
    }
}

let digital = createClock(DigitalClock, 12, 17);
digital.tick();
let analog = createClock(AnalogClock, 7, 32);
analog.tick();