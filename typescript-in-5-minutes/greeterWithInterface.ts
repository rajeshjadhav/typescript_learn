interface Person {
    fname: string;
    lname: string;
}

function greeter(person: Person) {
    return 'Hello, ' + person.fname + ' ' + person.lname;
}

let user = {
    fname: 'Rajesh',
    lname: 'Jadhav'
};

document.body.innerHTML = greeter(user);