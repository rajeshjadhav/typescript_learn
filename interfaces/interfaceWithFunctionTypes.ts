interface searchFunction {
    (source: string, substring: string)
}

let mySearch: searchFunction;
// mySearch = function (source: string, substring: string) {
//     let result = source.search(substring);
//     return result > -1;
// }

//  For function types to correctly type-check, 
// the names of the parameters do not need to match.

mySearch = function (src: string, sub: string): boolean {
    let result = src.search(sub);
    return result > -1;
}

console.log('search result:',mySearch('rajesh', 's'));